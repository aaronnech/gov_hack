const express = require('express');
const bodyParser = require('body-parser')
const path = require('path');
const Promise = require('bluebird');
const db = require('sqlite');

const AppDAO = require('./server/dao');
const ProgramModel = require('./server/ProgramModel');
const ApplicationModel = require('./server/ApplicationModel');

const DB_FILE = './database.sqlite3';
const app = express();

app.get('/api/programs', async (req, res, next) => { 
  const dao = new AppDAO(DB_FILE);
  const programs = new ProgramModel(dao);

  const results = await Promise.resolve(programs.getAll());
  res.json(results);
});

app.post('/api/application', async (req, res, next) => { 
  const firstName = req.param('first_name');
  const lastName = req.param('last_name');
  const appliedProgramIds = req.param('applied_program_ids');

  const dao = new AppDAO(DB_FILE);
  const programs = new ApplicationModel(dao);

  const results = await Promise.resolve(ApplicationModel.create(1, firstName, lastName, appliedProgramIds));
});

// Catch all, pass remaining get requests to the client router
app.use(express.static(path.join(__dirname, 'client', 'build')));
app.get('/*', async (req, res, next) => {
  res.sendFile(path.join(__dirname, 'client', 'build', 'index.html'));
});

Promise.resolve()
  .then(() => db.open(DB_FILE, { Promise }))
  .then(() => db.migrate({ force: 'last' })) 
  .catch((err) => console.error(err.stack))
  .finally(() => app.listen(process.env.PORT || 3000));