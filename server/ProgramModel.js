class ProgramModel {
  constructor(dao) {
    this.dao = dao
  }

  getAll() {
    return this.dao.all(`SELECT * FROM Program`, [])
  }
}

module.exports = ProgramModel;