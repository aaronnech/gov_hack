class ApplicationModel {
  constructor(dao) {
    this.dao = dao
  }

  create(userId, firstName, lastName, appliedProgramIds) {
    return this.dao.run(
      `INSERT INTO Application (user_id, first_name, last_name, applied_program_ids)
        VALUES (?, ?, ?, ?)`,
      [userId, firstName, lastName, appliedProgramIds]);
  }
}

module.exports = ApplicationModel;