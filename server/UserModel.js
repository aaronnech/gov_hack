const crypto = require('crypto');
const PASS_SALT = 'kas234234dhfkjs23423ahdfkjshdf324234234';

class UserModel {
  constructor(dao) {
    this.dao = dao
  }

  create(email, password) {
  	const hashPass = crypto.scrypt(password, PASS_SALT);
    return this.dao.run(
      `INSERT INTO User (email, password)
        VALUES (?, ?)`,
      [email, hashPass]);
  }

  get(email, password) {
  	const hashPass = crypto.scrypt(password, PASS_SALT);
    return this.dao.get(
      `SELECT * FROM User WHERE email = ? and password = ?`,
      [email, hashPass]);
  }
}

module.exports = UserModel;