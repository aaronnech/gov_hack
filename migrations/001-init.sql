-- Up 
CREATE TABLE User (
  id   INTEGER PRIMARY KEY,
  email TEXT UNIQUE,
  password TEXT NOT NULL
);

CREATE TABLE Application (
  id          INTEGER PRIMARY KEY,
  user_id  INTEGER NOT NULL,
  applied_program_ids TEXT NOT NULL,
  first_name TEXT NOT NULL,
  last_name TEXT NOT NULL
);

CREATE TABLE Program (
  id INTEGER PRIMARY KEY,
  department TEXT NOT NULL,
  program_name TEXT NOT NULL,
  link TEXT NOT NULL,
  description TEXT NOT NULL,
  photo_url TEXT NOT NULL,
  categories TEXT NOT NULL,
  competentcies TEXT NOT NULL,
  contact_email TEXT NOT NULL,
  contact_phone TEXT NOT NULL,
  min_age INTEGER NOT NULL,
  max_age INTEGER NOT NULL
);


INSERT INTO Program 
            (id, 
             department, 
             program_name, 
             link, 
             description, 
             photo_url,
             categories,
             competentcies, 
             contact_email, 
             contact_phone, 
             min_age, 
             max_age) 
VALUES (0, 
        'parks_and_recreation', 
        'YCTP (Youth Career Training Program)', 
		'http://www.seattle.gov/parks/about-us/work-with-us/job-opportunities-for-teens', 
		'A youth one-stop center located at WorkSource Affiliate Tukwila offering an array of programs for young adults ages 16 to 24 who have dropped out of high school. These programs focus on education, employment and leadership development. They also provide connections to youth programs, community resources for life stabilization, job readiness and placement services, and comprehensive case management.',
		'https://img.freepik.com/free-photo/diversity-casual-teenager-team-success-winning-concept_53876-23003.jpg?size=626&ext=jpg',
		'',
		'',
		'gwen.wessels@seattle.gov',
		'(206) 615-1727',
		14,
		24);


-- Down 
DROP TABLE User;
DROP TABLE Application;
DROP TABLE Program;