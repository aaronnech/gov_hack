import React from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import PageHeader from './view/PageHeader';

import Home from './routes/Home';
import ProgramDirectory from './routes/ProgramDirectory';
import ProgramDetail from './routes/ProgramDetail';
import ApplicationFormRoute from './routes/ApplicationFormRoute';

function AppRouter() {
  return (
    <Router>
      <PageHeader />
      <Route path="/" exact component={Home} />
      <Route path="/programs" exact component={ProgramDirectory} />
      <Route path="/program_details/:id" exact component={ProgramDetail} />
      <Route path="/application_form/" exact component={ApplicationFormRoute} />
    </Router>
  );
}

export default AppRouter;
