import React from 'react';
import ProgramList from '../view/ProgramList';
import ValuePropJumbotron from '../view/ValuePropJumbotron';
import Programs from '../YouthProgramData';

function Home() {
  const popularPrograms = Programs.slice(0, 3);
  const newPrograms = Programs.slice(4, 7);

  return (
    <div>
      <ValuePropJumbotron />
      <ProgramList title={'Popular Programs'} programs={popularPrograms} />
      <ProgramList title={'New Programs'} programs={newPrograms} />
    </div>
  );
}

export default Home;
