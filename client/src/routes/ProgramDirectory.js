import React from 'react';
import ProgramDirectoryJumbotron from '../view/ProgramDirectoryJumbotron';
import ProgramList from '../view/ProgramList';
import Programs from '../YouthProgramData';

function ProgramDirectory() {
  return (
    <div>
      <ProgramDirectoryJumbotron />
      <ProgramList title={'All Programs'} programs={Programs} />
    </div>
  );
}

export default ProgramDirectory;
