import React from 'react';
import Programs from '../YouthProgramData';
import DetailedProgramView from '../view/DetailedProgramView.react';

function ProgramDetail({match}) {
  const id = match.params.id;
  const program = Programs.find((program) => {
    return program.id === id;
  });

  if (!program) {
    return null;
  }

  return (
    <DetailedProgramView program={program} similarPrograms={Programs.slice(0,3)} />
  );
}

export default ProgramDetail;
