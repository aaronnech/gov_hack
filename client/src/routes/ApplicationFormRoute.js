import React from 'react';
import ApplicationFormView from '../view/ApplicationFormView.react';

function ApplicationFormRoute() {
  return (
    <ApplicationFormView />
  );
}

export default ApplicationFormRoute;
