const Competencies = Object.freeze({
    AWARENESS: 'Social and Cultural Awareness',
    COMMUNICATION: 'Communication',
    GROWTH: 'Growth Mindset',
    READINESS: 'Job and Life Readiness',
    THINKING: 'Critical Thinking',
});

const Department = Object.freeze({
    HUMAN_SERVICES_DEPARTMENT: 'Human Services Department',
    OFFICE_OF_ARTS_AND_CULTURE: 'Office of Arts & Culture',
    OFFICE_OF_ECONOMIC_DEVELOPMENT: 'Office of Economic Development',
    PARKS_AND_RECREATION: 'Parks and Receation',
    SEATTLE_PUBLIC_LIBRARY: 'Seattle Public Library',
});

const ProgramCategory = Object.freeze({
    ARTS_AND_CULTURE: 'Arts & Culture',
    CAREER_EXPLORATION: 'Career Exploration',
    EDUCATION: 'Education',
    HEALTH: 'Health',
    LEADERSHIP: 'Leadership',
    MENTORING: 'Mentoring',
    RECREATIONAL: 'Recreational',
});

const Programs = [
    {
        id: 'yctp',
        department: Department.PARKS_AND_RECREATION,
        programName: 'YCTP (Youth Career Training Program)',
        link: 'http://www.seattle.gov/parks/about-us/work-with-us/job-opportunities-for-teens',
        description: 'TEST DESCRIPTION A youth one-stop center located at WorkSource Affiliate Tukwila offering an array of programs for young adults ages 16 to 24 who have dropped out of high school. These programs focus on education, employment and leadership development. They also provide connections to youth programs, community resources for life stabilization, job readiness and placement services, and comprehensive case management.',
        photo: '/images/1.jpeg',
        categories: [ProgramCategory.CAREER_EXPLORATION],
        competentcies: [Competencies.AWARENESS, Competencies.COMMUNICATION, Competencies.GROWTH, Competencies.READINESS, Competencies.THINKING],
        contactInfo: {
            email: 'gwen.wessels@seattle.gov',
            phone: '(206) 615-1727',
        },
        age: {
            min: 14,
            max: 24,
        },
        interests: [],
    },
    {
        id: 'yes',
        department: Department.PARKS_AND_RECREATION,
        programName: 'YES! (Youth Engaged in Service!)',
        link: 'http://www.seattle.gov/parks/about-us/work-with-us/job-opportunities-for-teens',
        description: 'TEST DESCRIPTION A youth one-stop center located at WorkSource Affiliate Tukwila offering an array of programs for young adults ages 16 to 24 who have dropped out of high school. These programs focus on education, employment and leadership development. They also provide connections to youth programs, community resources for life stabilization, job readiness and placement services, and comprehensive case management.',
        photo: '/images/2.jpeg',
        categories: [ProgramCategory.EDUCATION, ProgramCategory.RECREATIONAL],
        competentcies: [Competencies.AWARENESS, Competencies.COMMUNICATION, Competencies.GROWTH, Competencies.READINESS, Competencies.THINKING],
        contactInfo: {
            email: 'gwen.wessels@seattle.gov',
            phone: '(206) 615-1727',
        },
        age: {
            min: 13,
            max: 19,
        },
        interests: [],
    },
    {
        id: 'ypar',
        department: Department.PARKS_AND_RECREATION,
        programName: 'YPAR (Youth Participatory Action Research)',
        link: 'http://www.seattle.gov/parks/about-us/work-with-us/job-opportunities-for-teens',
        description: 'TEST DESCRIPTION A youth one-stop center located at WorkSource Affiliate Tukwila offering an array of programs for young adults ages 16 to 24 who have dropped out of high school. These programs focus on education, employment and leadership development. They also provide connections to youth programs, community resources for life stabilization, job readiness and placement services, and comprehensive case management.',
        photo: '/images/3.jpeg',
        categories: [ProgramCategory.EDUCATION, ProgramCategory.LEADERSHIP],
        competentcies: [Competencies.AWARENESS, Competencies.COMMUNICATION, Competencies.GROWTH, Competencies.READINESS, Competencies.THINKING],
        contactInfo: {
            email: 'teen.programs@seattle.gov',
            phone: '(206) 684-4575',
        },
        age: {
            min: 12,
            max: 16,
        },
        interests: [],
    },
    {
        id: 'step',
        department: Department.PARKS_AND_RECREATION,
        programName: 'STEP (Student Teen Employment Preparation)',
        link: 'http://www.seattle.gov/parks/about-us/work-with-us/job-opportunities-for-teens',
        description: 'TEST DESCRIPTION A youth one-stop center located at WorkSource Affiliate Tukwila offering an array of programs for young adults ages 16 to 24 who have dropped out of high school. These programs focus on education, employment and leadership development. They also provide connections to youth programs, community resources for life stabilization, job readiness and placement services, and comprehensive case management.',
        photo: '/images/4.jpeg',
        categories: [ProgramCategory.CAREER_EXPLORATION, ProgramCategory.MENTORING],
        competentcies: [Competencies.AWARENESS, Competencies.COMMUNICATION, Competencies.GROWTH, Competencies.READINESS, Competencies.THINKING],
        contactInfo: {
            email: 'teen.programs@seattle.gov',
            phone: '(206) 684-4575',
        },
        age: {
            min: 14,
            max: 19,
        },
        interests: [],
    },
    {
        id: 'new1',
        department: Department.PARKS_AND_RECREATION,
        programName: 'New 1',
        link: 'http://www.seattle.gov/parks/about-us/work-with-us/job-opportunities-for-teens',
        description: 'New program 1 test description',
        photo: '/images/5.jpeg',
        categories: [ProgramCategory.CAREER_EXPLORATION],
        competentcies: [Competencies.AWARENESS, Competencies.COMMUNICATION, Competencies.GROWTH, Competencies.READINESS, Competencies.THINKING],
        contactInfo: {
            email: 'gwen.wessels@seattle.gov',
            phone: '(206) 615-1727',
        },
        age: {
            min: 14,
            max: 24,
        },
        interests: [],
    },
    {
        id: 'new2',
        department: Department.PARKS_AND_RECREATION,
        programName: 'New 2',
        link: 'http://www.seattle.gov/parks/about-us/work-with-us/job-opportunities-for-teens',
        description: 'New program 2 test description',
        photo: '/images/7.jpeg',
        categories: [ProgramCategory.EDUCATION, ProgramCategory.RECREATIONAL],
        competentcies: [Competencies.AWARENESS, Competencies.COMMUNICATION, Competencies.GROWTH, Competencies.READINESS, Competencies.THINKING],
        contactInfo: {
            email: 'gwen.wessels@seattle.gov',
            phone: '(206) 615-1727',
        },
        age: {
            min: 13,
            max: 19,
        },
        interests: [],
    },
    {
        id: 'new3',
        department: Department.PARKS_AND_RECREATION,
        programName: 'New 3',
        link: 'http://www.seattle.gov/parks/about-us/work-with-us/job-opportunities-for-teens',
        description: 'New program 3 test description',
        photo: '/images/8.jpeg',
        categories: [ProgramCategory.EDUCATION, ProgramCategory.LEADERSHIP],
        competentcies: [Competencies.AWARENESS, Competencies.COMMUNICATION, Competencies.GROWTH, Competencies.READINESS, Competencies.THINKING],
        contactInfo: {
            email: 'teen.programs@seattle.gov',
            phone: '(206) 684-4575',
        },
        age: {
            min: 12,
            max: 16,
        },
        interests: [],
    },
    {
        id: 'new4',
        department: Department.PARKS_AND_RECREATION,
        programName: 'New 4',
        link: 'http://www.seattle.gov/parks/about-us/work-with-us/job-opportunities-for-teens',
        description: 'New program 4 test description',
        photo: '/images/9.jpeg',
        categories: [ProgramCategory.CAREER_EXPLORATION, ProgramCategory.MENTORING],
        competentcies: [Competencies.AWARENESS, Competencies.COMMUNICATION, Competencies.GROWTH, Competencies.READINESS, Competencies.THINKING],
        contactInfo: {
            email: 'teen.programs@seattle.gov',
            phone: '(206) 684-4575',
        },
        age: {
            min: 14,
            max: 19,
        },
        interests: [],
    },
];

export default Programs;