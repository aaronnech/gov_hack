import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Jumbotron from 'react-bootstrap/Jumbotron';
import Button from 'react-bootstrap/Button';

function ValuePropJumbotron() {
  return (
    <Jumbotron fluid style={{color: 'white', backgroundImage: 'url(/images/main_background.png)', height: '550px',}}>
      <Container style={{height: '100%', display: 'flex', alignItems: 'center'}}>
        <Row>
          <Col>
            <h1>Welcome to the Youth Portal by the City of Seattle.</h1>
            <p>
              Research potential careers, find programs based on interested and background, and apply to programs using a simple common application.
            </p>
            <p>
              <Button
                variant="primary"
                style={{backgroundColor: '#faaf52', height: '36px', width:'180px', padding: '0'}}
                onClick={() => window.location.pathname = '/programs'}>
                See List of Programs
              </Button>
            </p>
          </Col>
          <Col />
        </Row>
      </Container>
    </Jumbotron>
  );
}

export default ValuePropJumbotron;
