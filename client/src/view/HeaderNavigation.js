import React from 'react';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';

function HeaderNavigation({signUpOnClick}) {
  function navBarOnSelect(key) {
    if (key === "signUp") {
      signUpOnClick(true);
    }
  }

  return (
    <Nav onSelect={selectedKey => navBarOnSelect(selectedKey)}>
      <Nav.Item>
        <Nav.Link>About</Nav.Link>
      </Nav.Item>
      <Nav.Item>
        <Nav.Link href="/programs/">Programs</Nav.Link>
      </Nav.Item>
      <Nav.Item>
        <Nav.Link>FAQ</Nav.Link>
      </Nav.Item>
      <Nav.Item>
        <Nav.Link>Contact</Nav.Link>
      </Nav.Item>
      <Nav.Item>
        <Nav.Link eventKey="signUp">Sign Up</Nav.Link>
      </Nav.Item>
    </Nav>
  );
}

export default HeaderNavigation;
