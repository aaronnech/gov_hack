import React from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import Form from 'react-bootstrap/Form';


function SignUpInModal(props) {
	return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered>
      <Modal.Header closeButton style={{border: 'none'}}>
        <Modal.Title
					id="contained-modal-title-vcenter"
					style={{color: '#faaf52', borderBottom: '1px solid #faaf52', marginLeft: '80px', padding: '4rem 4rem 1rem 0rem', width: '100%'}}>
          Youth Portal
        </Modal.Title>
      </Modal.Header>
      <Modal.Body
				style={{marginLeft: '100px', padding: '1rem 1rem 1rem 0rem',}}>

        <h4>Create an account to get started</h4>
        <p>
          In order to apply to programs, you will need to create an account.
        </p>

				<Form>
					<Form.Group controlId="signUpName">
						<Form.Label
							style={{}}>
							Full Name
						</Form.Label>
						<Form.Control type="text" placeholder="" style={{width: '50%'}}/>
					</Form.Group>

					<Form.Group controlId="signUpEmail">
						<Form.Label
							style={{}}>
							Email Address
						</Form.Label>
						<Form.Control type="email" placeholder="" style={{width: '50%'}}/>
					</Form.Group>

					<Form.Group controlId="signUpPassword">
						<Form.Label
							style={{}}>
							Password
						</Form.Label>
						<Form.Control type="password" placeholder="" style={{width: '50%'}}/>
					</Form.Group>
				</Form>

        <Button
					onClick={props.onHide}
					style={{backgroundColor: '#faaf52', height: '36px', width: '179px'}}>
					<div style={{marginTop: '-6px'}}>Create Account</div>
				</Button>

				<div style={{height: '100px', paddingTop: '10px'}}>
						Already registered?
						<span
							onClick={props.onHide}
							style={{color: '#faaf52', cursor: 'pointer', marginLeft: '4px'}}>
							Log in
						</span>
				</div>
      </Modal.Body>
    </Modal>
	);
}

export default SignUpInModal;
