import React from 'react';
import Card from 'react-bootstrap/Card';

type Props = {
  programId: string,
  programTitle: string,
  programDescription: string,
  programImageURL: string,
};

class ProgramTile extends React.Component<Props> {
  render() {
    const {programId, programTitle, programDescription, programImageURL} = this.props;
    const truncatedDesc = programDescription.length > 150
      ? programDescription.substring(0, 150) + '...'
      : programDescription;
    return (
      <Card
        style={{height: '400px', cursor: 'pointer', margin: '12px 0px'}}
        onClick={() => window.location.pathname = '/program_details/' + programId}>
        <Card.Img
          variant="top" src={programImageURL}
          style={{height: '200px'}}/>
        <Card.Body style={{height: '200px', overflow: 'hidden'}}>
          <Card.Title>{programTitle}</Card.Title>
          <Card.Text style={{overflow: 'hidden'}}>{truncatedDesc}</Card.Text>
        </Card.Body>
      </Card>);
  }
}

export default ProgramTile;
