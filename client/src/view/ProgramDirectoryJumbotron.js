import React from 'react';
import Container from 'react-bootstrap/Container';
import Jumbotron from 'react-bootstrap/Jumbotron';
import Button from 'react-bootstrap/Button';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';

function ProgramDirectoryJumbotron() {
  return (
    <Jumbotron style={{backgroundColor: 'white', textAlign: 'center', paddingBottom: '0px'}}>
      <Container>
            <h1>Youth Program Directory</h1>
            <p>
              Search for keywords or filter based on your need.
            </p>
            <InputGroup style={{width: '488px', height: '36px', margin: '30px auto 20px auto'}}>
              <FormControl placeholder="Keywords" />
            </InputGroup>
            <p>
              <Button 
                variant="primary" 
                style={{backgroundColor: '#faaf52', height: '36px', width:'180px', padding: '0'}}
              >
                Search
              </Button>
            </p>
      </Container>
    </Jumbotron>
  );
}

export default ProgramDirectoryJumbotron;
