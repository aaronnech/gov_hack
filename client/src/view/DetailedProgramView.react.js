import React from 'react';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import Programs from '../YouthProgramData';
import ProgramList from '../view/ProgramList';


function DetailedProgramView({program, similarPrograms}) {

  return (
    <>
      <Card style={{margin: '40px auto', width: '1200px'}}>
        <Card.Img variant="top" src={program.photo} style={{maxHeight: '700px'}} />
        <Card.Body>

          <Card.Title style={{fontSize: '28px'}}>{program.programName}</Card.Title>
          <Card.Text>{program.description}</Card.Text>

          <Card.Text style={{padding: '12px 0px'}}>
            <span
              style={{color: '#a6aaae', fontSize: '16px', fontWeight: '550', marginRight: '8px'}}>
              CATEGORY:
            </span>
            <span style={{fontWeight: '550'}}>{program.categories.join(', ')}</span>
          </Card.Text>

          <Card.Text>
            <div
              style={{color: '#a6aaae', fontSize: '20px', fontWeight: '550', padding: '10px 0px'}}>
              Offered By
            </div>
            <div style={{fontSize: '23px', fontWeight: '550', padding: '10px 0px'}}>
              {program.department}
            </div>
          </Card.Text>
          <div style={{borderBottom: '1px solid lightGrey', height: '10px'}} />

          <Card.Text>
            <div
              style={{color: '#a6aaae', fontSize: '20px', fontWeight: '550', padding: '10px 0px'}}>
              Ages
            </div>
            <div style={{fontSize: '23px', fontWeight: '550', padding: '10px 0px'}}>
              {program.age.min} - {program.age.max}
            </div>
          </Card.Text>
          <div style={{borderBottom: '1px solid lightGrey', height: '10px'}} />

          <Card.Text>
            <div
              style={{color: '#a6aaae', fontSize: '20px', fontWeight: '550', padding: '10px 0px'}}>
              Fees
            </div>
            <div style={{fontSize: '23px', fontWeight: '550', padding: '10px 0px'}}>
              Free
            </div>
          </Card.Text>
          <div style={{borderBottom: '1px solid lightGrey', height: '10px'}} />

          <Card.Text>
            <div
              style={{color: '#a6aaae', fontSize: '20px', fontWeight: '550', padding: '10px 0px'}}>
              Eligibility
            </div>
            <div style={{fontSize: '23px', fontWeight: '550', padding: '10px 0px'}}>
              ???
            </div>
          </Card.Text>
          <div style={{borderBottom: '1px solid lightGrey', height: '10px'}} />
          <Button
            style={{backgroundColor: '#faaf52', height: '36px', margin: '40px 42%', width: '179px'}}
            onClick={() => window.location.pathname = '/application_form'}
          >
            <div style={{marginTop: '-6px'}}>Apply Now</div>
          </Button>
        </Card.Body>
      </Card>

      <ProgramList title={'Similar Programs'} programs={similarPrograms} />
    </>
  );
}

export default DetailedProgramView;
