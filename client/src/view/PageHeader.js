import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Image from 'react-bootstrap/Image';

import HeaderNavigation from './HeaderNavigation';
import SignUpInModal from '../view/SignUpInModal';

function PageHeader() {
  const [showModal, setShowModal] = React.useState(false);

  return (
    <>
      <Container>
        <Row>
          <Col>
            <div onClick={() => window.location.pathname = '/'} style={{cursor: 'pointer', width: '160px'}}>
              <Image
                src="http://www.seattle.gov/Images/Council/Logos/City-of-Seattle-Logo_Black-on-Transparent.png"
                style={{height: '35px', width: '35px'}}/>
              <span style={{fontWeight: 'bold', marginLeft: '8px', verticalAlign: 'middle'}}>
                Youth Portal
              </span>
          </div>
          </Col>
          <Col>
            <HeaderNavigation signUpOnClick={(value) => setShowModal(value)} />
          </Col>
        </Row>
      </Container>

      <SignUpInModal 
        show={showModal}
        onHide={() => setShowModal(false)} 
      />
    </>
  );
}

export default PageHeader;
