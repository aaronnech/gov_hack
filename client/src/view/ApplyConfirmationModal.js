import React from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import Form from 'react-bootstrap/Form';
import Image from 'react-bootstrap/Image';



function SignUpInModal(props) {
	return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered>
      <Modal.Header closeButton style={{border: 'none'}}>
        <Modal.Title
					id="contained-modal-title-vcenter"
					style={{color: '#faaf52', borderBottom: '1px solid #faaf52', marginLeft: '80px', padding: '4rem 4rem 1rem 0rem', width: '100%'}}>
          Youth Portal
        </Modal.Title>
      </Modal.Header>
      <Modal.Body
				style={{marginLeft: '100px', padding: '1rem 1rem 1rem 0rem',}}>

        <span>
          <Image
            src="/images/greencheck.png"
            style={{display: 'inline', height: '35px', verticalAligh: 'top', width: '35px'}}/>
          <h4  style={{display: 'inline'}}>Thanks for applying!</h4>
        </span>
        <p>
          We will send you a confirmation email shortly. <br/>
          Upon reviewing your application, we will also let you know what's next.
        </p>

        <Button
					onClick={props.onHide}
					style={{backgroundColor: '#faaf52', height: '36px', marginBottom: '10px', width: '179px'}}>
					<div style={{marginTop: '-6px'}}>Confirm</div>
				</Button>

      </Modal.Body>
    </Modal>
	);
}

export default SignUpInModal;
