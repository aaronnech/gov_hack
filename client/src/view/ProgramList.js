import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import ProgramTile from './ProgramTile'

function ProgramList({title, programs}) {

  const NUM_TILE_PER_ROW = 3;

  return (
    <Container style={{marginBottom: '32px'}}>
      <Row style={{fontSize: '28px', fontWeight: 'bold', padding: '8px 0px' }}>
        <Col>{title}</Col>
      </Row>
      {getProgramGrid()}
    </Container>
  );

  function getProgramGrid() {
    const numToFill = NUM_TILE_PER_ROW - programs.length % NUM_TILE_PER_ROW;
    programs = programs.concat(Array(numToFill).fill(null));

    const numRows = Math.ceil(programs.length / NUM_TILE_PER_ROW);
    const grid = Array(numRows).fill().map((_, rowIndex) => (
      <Row>
        {
          programs.slice(rowIndex * NUM_TILE_PER_ROW, (rowIndex * NUM_TILE_PER_ROW) + NUM_TILE_PER_ROW).map(program => (
            program ?
              <Col>
                <ProgramTile
                  programId={program.id}
                  programTitle={program.programName}
                  programDescription={program.description}
                  programImageURL={program.photo}
                />
              </Col>
            :
              <Col />
          ))
        }
      </Row>
    ));

    return grid;
  }
}

export default ProgramList;
