import React from 'react';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import Programs from '../YouthProgramData';
import Form from 'react-bootstrap/Form';
import ApplyConfirmationModal from '../view/ApplyConfirmationModal';



type Props = {
};

type State = {
  showConfirmModal: boolean,
}

class ApplicationFormView extends React.Component<Props, State> {

  state = {
    showConfirmModal: false,
  }

  renderLayers() {
    if (this.state.showConfirmModal) {
      console.log('???');
      return (
        <ApplyConfirmationModal
        show={true}
        onHide={() => this.setState({showConfirmModal: false})} />
      );
    }
  }

  render() {
    return (
      <div>
      {this.renderLayers()}
      <Card style={{height: '2790px', margin: '40px auto', width: '1200px'}}>
        <Card.Img variant="top" src={'/images/formimage.png'} />
        <Card.Body>

          <Card.Title
            style={{fontSize: '25px', fontWeight: '550'}}>
            Basic Information
          </Card.Title>
          <Card.Text
            style={{paddingBottom: '12px'}}>
            Let us know who you are so that we can better match you with a program that suits you
          </Card.Text>

          <Form>
            <Form.Group controlId="formName">
              <Form.Label
                style={{color: '#a6aaae',fontSize: '20px', fontWeight: '550', padding: '12px 0px 8px'}}>
                Full Name
              </Form.Label>
              <Form.Control type="text" placeholder="" />
            </Form.Group>

            <Form.Group controlId="formBirthdate">
              <Form.Label
                style={{color: '#a6aaae',fontSize: '20px', fontWeight: '550', padding: '12px 0px 8px'}}>
                Birth Date
              </Form.Label>
              <Form.Control type="date" placeholder="" />
            </Form.Group>

            <Form.Group controlId="formSchoolName">
              <Form.Label
                style={{color: '#a6aaae',fontSize: '20px', fontWeight: '550', padding: '12px 0px 8px'}}>
                School Name
              </Form.Label>
              <Form.Control type="text" placeholder="" />
            </Form.Group>

            <Form.Group controlId="formAddress">
              <Form.Label
                style={{color: '#a6aaae',fontSize: '20px', fontWeight: '550', padding: '12px 0px 8px'}}>
                Address
              </Form.Label>
              <Form.Control type="text" placeholder="" />
            </Form.Group>

            <Form.Group controlId="formEmail">
              <Form.Label
                style={{color: '#a6aaae',fontSize: '20px', fontWeight: '550', padding: '12px 0px 8px'}}>
                Email Address
              </Form.Label>
              <Form.Control type="email" placeholder="" />
            </Form.Group>

            <Form.Group controlId="formPhoneNumber">
              <Form.Label
                style={{color: '#a6aaae',fontSize: '20px', fontWeight: '550', padding: '12px 0px 8px'}}>
                Phone Number
              </Form.Label>
              <Form.Control type="tel" placeholder="" />
            </Form.Group>

            <Form.Group controlId="formEmergencyContact">
              <Form.Label
                style={{color: '#a6aaae',fontSize: '20px', fontWeight: '550', padding: '12px 0px 8px'}}>
                Emergency Contact Person Name
              </Form.Label>
              <Form.Control type="text" placeholder="" />
            </Form.Group>

            <div style={{borderBottom: '1px solid lightGrey', height: '48px'}} />

            <Card.Title
              style={{fontSize: '25px', fontWeight: '550', paddingTop: '48px'}}>
              Educational History
            </Card.Title>
            <Card.Text>
              Please select your employement interests. We will make every effort to place selected applicants in worksites that are within their areas of interest. If you are selected into the program, you may be placed in a worksite that is not in your selected area of interest.
            </Card.Text>

            <Form.Group controlId="formEducationLevel">
              <Form.Label
                style={{color: '#a6aaae',fontSize: '20px', fontWeight: '550', padding: '12px 0px 8px'}}>
                Education Level
              </Form.Label>
              <Form.Control as="select">
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
              </Form.Control>
            </Form.Group>

            <Form.Group controlId="formEducationStatus">
              <Form.Label
                style={{color: '#a6aaae',fontSize: '20px', fontWeight: '550', padding: '12px 0px 8px'}}>
                Education Status
              </Form.Label>
              <Form.Control as="select">
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
              </Form.Control>
            </Form.Group>

            <Form.Group controlId="formEducationCurrently">
              <Form.Label
                style={{color: '#a6aaae',fontSize: '20px', fontWeight: '550', padding: '12px 0px 8px'}}>
                Currently Attending School?
              </Form.Label>
              <Form.Control as="select">
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
              </Form.Control>
            </Form.Group>

            <div style={{borderBottom: '1px solid lightGrey', height: '48px'}} />

            <Card.Title
              style={{fontSize: '25px', fontWeight: '550', paddingTop: '48px'}}>
              Employment Interests
            </Card.Title>
            <Card.Text>
              Please select your employement intersts. We will make every effort to place selected applicants in worksites that are within their areas of interst. If you are selected into the program, you may be placed in a worksite that is not in your selected area of interest.
            </Card.Text>

            <Form.Group controlId="formJobInterest1">
              <Form.Label
                style={{color: '#a6aaae',fontSize: '20px', fontWeight: '550', padding: '12px 0px 8px'}}>
                1st Job Interest
              </Form.Label>
              <Form.Control type="text" placeholder="" />
            </Form.Group>

            <Form.Group controlId="formJobInter3st2">
              <Form.Label
                style={{color: '#a6aaae',fontSize: '20px', fontWeight: '550', padding: '12px 0px 8px'}}>
                2nd Job Interest
              </Form.Label>
              <Form.Control type="text" placeholder="" />
            </Form.Group>

            <Form.Group controlId="formJobInterest3">
              <Form.Label
                style={{color: '#a6aaae',fontSize: '20px', fontWeight: '550', padding: '12px 0px 8px'}}>
                3rd Job Interest
              </Form.Label>
              <Form.Control type="text" placeholder="" />
            </Form.Group>

            <Button
              style={{backgroundColor: '#faaf52', height: '36px', margin: '40px 42%', width: '179px'}}
              type="button">
              <div onClick={() => this.setState({showConfirmModal: true})} style={{marginTop: '-6px'}}>Save Application</div>
            </Button>

          </Form>

        </Card.Body>

      </Card>
    </div>);
  }
}

export default ApplicationFormView;
